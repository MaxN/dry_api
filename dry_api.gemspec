$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "dry_api/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "dry_api"
  spec.version     = DryApi::VERSION
  spec.authors     = ["snake"]
  spec.email       = ["max@snakelab.cc"]
  spec.homepage    = "http://snakelab.cc"
  spec.summary     = "Dont repeat yourself with api"
  spec.description = "Gem contains minimum code needed to create api backend server"
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", "~> 5.2.0"
  spec.add_dependency "json-jwt"
  spec.add_dependency "kaminari"
 

  spec.add_development_dependency "sqlite3"
end
