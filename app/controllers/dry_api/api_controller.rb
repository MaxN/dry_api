module DryApi
  class ApiController < ActionController::API
    include ApiHelpers
    include JwtAuthHelper
    around_action :wrap_exception

    def sign_in
      if get_params[:email].present?
        if get_params[:jwt_token].present?
          unless validate_jwt_token(get_params[:jwt_token], get_params[:email])
            render_error 'wrong token'
            return
          end
          begin
            yield get_params[:email] if block_given?
          rescue => e
            render_error e.message
          end
        else
          render_error('no jwt_token param')
        end
      else
        render_error('no email params')
      end
    end

    def index
      page = get_params[:page] || 1
      per = get_params[:per] || 50
      id = get_params[:id]

      where = yield get_params
      # must be in mseconds since epoch in utc format
      timestamp = get_params[:timestamp]
      if timestamp.present?
        timestamp = Time.from_msec(timestamp).to_datetime
        # puts " server #{(timestamp).to_s(:iso8601ms)}"
        where = where.where('updated_at > :timestamp', timestamp: timestamp)
      end
      list = if where.respond_to? :page
        where.page(page).per(per)
      else
        where
      end
      render_resp list, 200
    end
    def show
      render_resp yield get_params, 200
    end

  protected
    def get_params *permits
      params.permit(%i[email jwt_token token page per id timestamp] + permits)
    end

    def wrap_exception
      yield
    rescue => e
      raise if Rails.env.test?
      Rails.logger.fatal e.message
      render_error e.message

    end

    def render_resp(list = nil, code = 200)
      resp = {}
      unless list.nil?
        list = [list] unless list.respond_to? :to_a
        resp = { data: list.to_a.map(&:to_hash), meta: {} }

        if get_pagination(list).present?
          resp[:meta] = { pagination: get_pagination(list) }
        end
        
        resp.delete(:meta) if resp[:meta].empty?
      end
      render json: resp, status: code
    end
    def get_pagination(list)
      list.respond_to?(:last_page?) && !list.empty? && !list.last_page? && { page: list.current_page, total_count: list.total_count } || nil
    end
  end
end