
require 'json/jwt'
require 'net/http'
require 'json'

module DryApi::JwtAuthHelper
  def test_token? token
    Rails.env.test? #&& ENV["TAXIBUS_JWT_TEST_TOKEN"] == token
  end

  def validate_jwt_token token, email
    return true if test_token? token
    begin
      jwt = JSON::JWT.decode token, :skip_verification  
      kid = jwt.header["kid"]
      public_cert = get_public_cert kid 
      JSON::JWT.decode token, OpenSSL::X509::Certificate.new(public_cert).public_key
      token_email = jwt["email"]
    rescue => e
      raise if Rails.env.test?
      Rails.logger.fatal "unable to validate jwt token: #{e}"
      return false
    end
    email.present? && token_email.downcase == email.downcase
  end

  def get_public_cert kid
    uri = URI('https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com')
    response = Net::HTTP.get(uri)
    j = JSON.parse(response)
    j[kid]
  end

end