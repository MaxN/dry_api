module DryApi::ApiHelpers
  def build_error(title)
    { title: title }
  end

  def build_errors(arr)
    ret = []
    arr.each { |msg| ret << build_error(msg) }
    { errors: ret }
  end

  def render_error(msg)
    render json: build_errors([msg]), status: 404
  end

  def render_not_found
    render json: {}, status: 404
  end

  def render_ver_outdated
    render json: build_error('version is outdated'), status: 409
  end
end
