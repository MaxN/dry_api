require 'test_helper'

module DryApi
  class ApiControllerTest < ActionDispatch::IntegrationTest
    include Engine.routes.url_helpers
 
    setup do
      @routes = Engine.routes
    end
 
    test 'sing_in without token' do
      post sign_in_path    
      assert_response :not_found
      json = JSON.parse @response.body

    end
    test 'sign_in with test token' do
      post sign_in_path(email: "max.nedelchev@gmail.com", jwt_token: ENV["TAXIBUS_JWT_TEST_TOKEN"])
      assert_response :success
    end
  end
end