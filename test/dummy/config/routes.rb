Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mount DryApi::Engine => "/dry_api"
  scope '/api' do
  	post :sign_in, to: 'api#sign_in'
  end
end
